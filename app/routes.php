<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home');
});

Route::post('/contact', function () {
	$data = Input::all();

	Mail::send('emails.contact', $data, function ($message) use ($data) {
		$message
			->to('neil@neilkistner.com', 'Neil Kistner')
			->from($data['email'], $data['name'])
			->subject('New contact received!');
	});
});
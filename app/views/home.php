<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Neil Kistner</title>
        <meta name="description" content="St. Louis, MO web developer Neil Kistner's personal portfolio website.">
        <meta name="viewport" content="width=device-width">
        <meta name="google-site-verification" content="_UVVefK9JYO2DkR-jNwUaK0b7r-YnX4kZ_Zj118Oadc" />
        <script defer src="js/vendor/modernizr-2.6.2.min.js"></script>

        <link rel="author" href="http://plus.google.com/112662872265561195218"/>
        <link rel="publisher" href="http://plus.google.com/112662872265561195218"/>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <main>
            <img src="img/me.png" width="150" height="150" alt="Me" class="me" />
            <header>Neil Kistner</header>
            <hr class="styled" />
            <section>
                A husband &amp; father, front end developer at <a href="//safetynational.com" target="_blank" rel="external">Safety National</a>, cat lover, and avid gamer.
            </section>
            <ul class="social">
                <li><a href="//github.com/wyze" target="_blank" rel="external" data-social="GitHub"><i class="icon-github icon-3x"></i></a></li>
                <li><a href="//linkedin.com/in/neilkistner" target="_blank" rel="external" data-social="LinkedIn"><i class="icon-linkedin icon-3x"></i></a></li>
                <li><a href="//twitter.com/wyze" target="_blank" rel="external" data-social="Twitter"><i class="icon-twitter icon-3x"></i></a></li>
                <li><a href="//plus.google.com/112662872265561195218" target="_blank" rel="external" data-social="Google+"><i class="icon-google-plus icon-3x"></i></a></li>
                <li><a href="javascript:"><i class="icon-envelope icon-3x"></i></a></li>
            </ul>
        </main>
        <div class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Contact Me</h3>
            </div>
            <div class="modal-body">
                <form class="form-inline">
                    <div class="control-group d-ib">
                        <label class="control-label" for="contactName">Name:</label>
                        <span class="help-inline help-name"></span>
                        <div class="controls">
                            <input class="input-block-level" type="text" name="name" id="contactName" required data-minlength="4">
                        </div>
                    </div>
                    <div class="control-group d-ib ml">
                        <label class="control-label" for="contactEmail">Email:</label>
                        <span class="help-inline help-email"></span>
                        <div class="controls">
                            <input class="input-block-level" type="text" name="email" id="contactEmail" required data-minlength="6" data-email="true">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="contactMessage">Message:</label>
                        <span class="help-inline help-message"></span>
                        <div class="controls">
                            <textarea class="input-block-level" rows="8" name="msg" id="contactMessage" required data-minlength="10"></textarea>
                        </div>
                    </div>
                </form>
                <div class="thank-you">
                    <h2>Thanks<span class="highlight">.</span></h2>
                    <p>
                        Your email has been successfully sent. I will be in touch soon, usually within 48 hours.
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:" class="btn btn-primary disabled">Send Message</a>
            </div>
        </div>
        <footer>Copyright &copy; 2010-2013 Neil Kistner | All Rights Reserved</footer>
        <script defer src="//ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
        <script defer src="js/plugins.js"></script>
        <script defer src="js/main.js"></script>
        <script defer>
            var _gaq=[['_setAccount','UA-4907572-6'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';g.async=true;
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
        <link rel="stylesheet" href="css/main.min.css">
    </body>
</html>

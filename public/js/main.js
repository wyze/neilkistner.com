$(function () {
	var $modal = $('.modal');

	$('.me').corner('100px');

	$('.social').on('click', 'a:not([data-social])', function () {
		$modal.modal();
		_gaq.push(['_trackEvent', 'Contact', 'Click', 'Form Presented']);
	});

	$('.social').on('click', 'a[data-social]', function () {
		_gaq.push(['_trackEvent', 'Social Media', 'Click', $(this).data('social')]);
	});

	$modal
		.on('keydown keyup', 'input, textarea', function () {
			var $this = $(this),
				$group = $this.closest('.control-group'),
				$btn = $modal.find('.btn-primary');

			$btn.addClass('disabled');

			if ($this.is('[required]') && $this.val().trim().length === 0) {
				$group.addClass('error');
				$group.find('.help-inline').text('This field is required.');

				return;
			} else {
				$group.removeClass('error');
				$group.find('.help-inline').text('');
			}

			if ($this.is('[data-minlength]') && $this.val().trim().length < +$this.data('minlength')) {
				$group.addClass('error');
				$group.find('.help-inline').text('This field has a minimum of ' + $this.data('minlength') + ' characters.');

				return;
			} else {
				$group.removeClass('error');
				$group.find('.help-inline').text('');
			}

			if ($this.is('[data-email]') && !/^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/.test($this.val())) {
				$group.addClass('error');
				$group.find('.help-inline').text('Please use a valid email address.');

				return;
			} else {
				$group.removeClass('error');
				$group.find('.help-inline').text('');
			}

			if ($modal.find('.error').length === 0 && $modal.find('input, textarea').filter(function () { return $(this).is('[required]') && $(this).val().trim().length === 0; }).length === 0)
				$btn.removeClass('disabled');
		})
		.on('click', '.btn-primary', function () {
			var $this = $(this);

			if ($this.is('.disabled'))
				return;

			$this.addClass('disabled');

			$.post('/contact', $modal.find('form').serialize(), function (data) {
				$modal
					.find('form').fadeOut(function () {
						$modal.find('.thank-you').fadeIn();
						$modal
							.find('.modal-header').css('border', 'none')
							.find('h3').text('')
							.end().end()
							.find('.modal-footer').css({ border: 'none', background: 'none' })
							.find('.btn-primary').hide();
					});
				_gaq.push(['_trackEvent', 'Contact', 'Click', 'Form Submitted']);
			})
			.always(function () {
				$this.removeClass('disabled');
			})
		})
		.on('hidden', function () {
			$modal
				.find('.error').removeClass('.error').end()
				.find('.help-inline').text('').end()
				.find('input, textarea').val('').end()
				.find('form').show().end()
				.find('.thank-you').hide().end()
				.find('.modal-header').css('border-bottom', '1px solid #eee')
				.find('h3').text('Contact Me').end().end()
				.find('.modal-footer').css({ 'border-top': '1px solid #ddd', background: '#f5f5f5' })
				.find('.btn-primary').addClass('disabled').show();
		});
});